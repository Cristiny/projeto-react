import React, {useState, useEffect} from 'react'
import Imagens6 from '../imagens/not-found.svg'
import Api from './api.js'
import './moedas.css'

function Moedas() {
    const [stado, setStatus] = useState([])
    useEffect(() => {
        Api(setStatus)
    },[])
    return(
        <>
            <h1 class="cabecalho">Cotação das Moedas</h1>
            {stado.map(card => {
                return (
                    <div class="corpo">
                        <h3>{card.code}</h3>
                        <h3>{card.name}</h3>
                        <h3>{card.high}</h3>
                        <h3>{stado.low}</h3>
                        <h3>{card.create_date}</h3>
                    </div>
                )
            })}
        </>
    )
}
export default Moedas