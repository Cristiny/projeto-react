import axios from 'axios'

const api = axios.create({
    baseURL: 'https://economia.awesomeapi.com.br'
})

function Api(setStatus)  {
    api.get("/json/all")
        .then(res => {
            let list = []
            Object.keys(res.data).forEach(key => {
                list.push(res.data[key])
            })
            setStatus(list)
        })
        .catch(erro => {
            console.log(erro);
        })
}
export default Api